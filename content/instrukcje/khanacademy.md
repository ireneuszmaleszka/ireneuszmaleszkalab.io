+++
title =  "Khan Academy Instrukcja"
date = 2020-03-22T20:46:12+01:00
tags = []
featured_image = ""
description = ""
summary = "Instrukcje zalogowania się do portalu KhanAcademy"
+++
- Wchodzimy na stronę https://pl.khanacademy.org

- Klikamy przycisk `Zaloguj się`

![Instrukcja - Krok 1](/instrukcje/khanacademy/instrukcja1.png)

- W nowo otwartym oknie wpisujemy login (1) i hasło (2) otrzymane poprzez dziennik elektroniczny

![Instrukcja - Krok 2](/instrukcje/khanacademy/instrukcja2.png)

- Po zalogowaniu się zobaczymy pulpit ucznia, klikając na przycisk `Zadania domowe` (1) możemy zobaczyć zagadnienia do nauki, kilkając przycisk `Start` rozpoczynamy naukę (2)

![Instrukcja - Krok 3](/instrukcje/khanacademy/instrukcja3.png)
